## Apollo升级改造





#### Apollo改造有序本地缓存

```java


// 获取
private Map<String, String> getNamespaceItems(Namespace namespace) {
            //原写法
            //List<Item> items = itemService.findItemsWithoutOrdered(namespace.getId());
            //修改后
            List<Item> items = itemService.findItemsWithOrdered(namespace.getId());
            Map<String, String> configurations = new HashMap<>();
            for (Item item : items) {
                if (StringUtils.isEmpty(item.getKey())) {
                    continue;
                }
                configurations.put(item.getKey(), item.getValue());
            }
            return configurations;
}



public List<Item> findItemsWithoutOrdered(Long namespaceId) {
  	List<Item> items = itemRepository.findByNamespaceId(namespaceId);
  	if (items == null) {
      	return Collections.emptyList();
    }
  	return items;
}

public List<Item> findItemsWithOrdered(Long namespaceId) {
  	List<Item> items =                                                                    			itemRepository.findByNamespaceIdOrderByLineNumAsc(namespaceId);
  	if (items == null) {
      	return Collections.emptyList();
    }
  	return items;
}




private Map<String, String> mergeConfiguration(
  	Map<String, String> baseConfigurations,Map<String, String> coverConfigurations) {
  	//原写法
  	//Map<String, String> result = new HashMap<>();     
  	//使用LinkedHashMap保证按放入循序取出
  	Map<String, String> result2 = new LinkedHashMap<>();
  	for (Map.Entry<String, String> entry : baseConfigurations.entrySet()) {
      	result.put(entry.getKey(), entry.getValue());
    }
  	for (Map.Entry<String, String> entry : coverConfigurations.entrySet()) {
      	result.put(entry.getKey(), entry.getValue());
    }
  	return result;
}





Map<String, String> mergeReleaseConfigurations(List<Release> releases) {
    //原写法
    //Map<String, String> result = Maps.newHashMap();
    //使用LinkedHashMap保证按放入循序取出
    Map<String, String> result = Maps.newLinkedHashMap();
    for (Release release : Lists.reverse(releases)) {
        result.putAll(gson.fromJson(release.getConfigurations(),                                                             configurationTypeReference));
    }
    return result;
}





public void onRepositoryChange(String namespace, Properties newProperties) {
  	if (newProperties.equals(m_fileProperties)) {
      	return;
    }
  	Properties newFileProperties = new Properties();
  	newFileProperties.putAll(newProperties);
  	updateFileProperties(newFileProperties, m_upstream.getSourceType());
  	this.fireRepositoryChange(namespace, newProperties);
}
        
        
        
        
//LocalFileConfigRepository
public void onRepositoryChange(String namespace, Properties newProperties) {
  	if (newProperties.equals(m_fileProperties)) {
      	return;
    }
  	//原写法
  	//Properties newFileProperties = new Properties();
  	//自实现有序的Properties
  	Properties newFileProperties = new LinkedProperties();
  	newFileProperties.putAll(newProperties);
  	updateFileProperties(newFileProperties, m_upstream.getSourceType());
  	this.fireRepositoryChange(namespace, newProperties);
}
        
        
//RemoteConfigRepository
public Properties getConfig() {
  	if (m_configCache.get() == null) {
      this.sync();
    }
  	return transformApolloConfigToProperties(m_configCache.get());
}

        
private Properties transformApolloConfigToProperties(ApolloConfig apolloConfig) {
  	//原写法
  	//Properties newFileProperties = new Properties();
  	//自实现有序的Properties
  	Properties newFileProperties = new LinkedProperties();
  	Properties result = new Properties();
  	result.putAll(apolloConfig.getConfigurations());
  	return result;
}
        

```





#### Apollo内部Eureka改用独立Eureka

```java
#apollo-configservice bootstrap.yml
eureka:
  instance:
    hostname: ${hostname:localhost}
    preferIpAddress: true
    status-page-url-path: /info
    health-check-url-path: /health
  server:
    peerEurekaNodesUpdateIntervalMs: 60000
    enableSelfPreservation: false
  client:
    serviceUrl:
      defaultZone: http://${eureka.instance.hostname}:8080/eureka/
    healthcheck:
      enabled: true
    eurekaServiceUrlPollIntervalSeconds: 60
    
    
#apollo-adminservice bootstrap.yml    
eureka:
    instance:
    hostname: ${hostname:localhost}
    preferIpAddress: true
    status-page-url-path: /info
    health-check-url-path: /health
    client:
    serviceUrl:
    defaultZone: http://${eureka.instance.hostname}:8080/eureka/
    healthcheck:
    enabled: true
    eurekaServiceUrlPollIntervalSeconds: 60    



//@EnableEurekaServer
@EnableEurekaClient
@EnableAspectJAutoProxy
@EnableAutoConfiguration // (exclude = EurekaClientConfigBean.class)
@Configuration
@EnableTransactionManagement
@PropertySource(value = {"classpath:configservice.properties"})
@ComponentScan(basePackageClasses = {ApolloCommonConfig.class,
                                     ApolloBizConfig.class,
                                     ConfigServiceApplication.class,
                                     	ApolloMetaServiceConfig.class})
public class ConfigServiceApplication {

  public static void main(String[] args) throws Exception {
    	SpringApplication.run(ConfigServiceApplication.class, args);
  }

}    


<dependency>
   <groupId>com.ctrip.framework.apollo</groupId>
   <artifactId>apollo-core</artifactId>
   <version>RELEASE</version>
</dependency>
```

